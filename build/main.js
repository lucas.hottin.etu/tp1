"use strict";

var html = "";
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
data.sort(function (a, b) {
  if (a.price_small > b.price_small) return 1;
  if (a.price_small < b.price_small) return -1;
  if (a.price_large > b.price_large) return 1;
  if (a.price_large < b.price_large) return -1;
  return 0;
}); // let data2 = data.filter(({name}) => name.split("i").length !=2);
// data.forEach(({name,image,price_small,price_large}) => {
//     html += `<article class="pizzaThumbnail"><a href="${image}"><img src="${image}" /><section><h4>${name}</h4><ul><li>Prix petit format : ${price_small} €</li><li>Prix grand format : ${price_large} €</li></ul></section></a></article>`
// });
// const map = data.map(({name,image,price_small,price_large}) => `<article class="pizzaThumbnail"><a href="${image}"><img src="${image}" /><section><h4>${name}</h4><ul><li>Prix petit format : ${price_small} €</li><li>Prix grand format : ${price_large} €</li></ul></section></a></article>`)
// html = map.join(''); 

html = data.reduce(function (ac, _ref) {
  var name = _ref.name,
      image = _ref.image,
      price_small = _ref.price_small,
      price_large = _ref.price_large;
  return ac + "<article class=\"pizzaThumbnail\"><a href=\"".concat(image, "\"><img src=\"").concat(image, "\" /><section><h4>").concat(name, "</h4><ul><li>Prix petit format : ").concat(price_small, " \u20AC</li><li>Prix grand format : ").concat(price_large, " \u20AC</li></ul></section></a></article>");
}, "");
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map